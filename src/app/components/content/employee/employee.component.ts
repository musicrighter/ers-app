import { Component, OnInit } from '@angular/core';
import { Data } from '../../../app.provider';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  public body: string[];
  public subSet: string[];

  public constructor(private data: Data) {
    const obj = JSON.parse(JSON.stringify(this.data.storage));
    this.body = JSON.parse(obj.body);
    this.subSet = this.body;
  }

  setSort(event: any) {
    console.log(event.target.value);
    const builder: string[] = [];
    if (event.target.value === 'all') {
      this.subSet = this.body;
    } else {
      for (let index = 0; index < this.body.length; index++) {
        const status = this.body[index].split('status: ')[1].split(',')[0];
        if (status == event.target.value) {
          builder.push(this.body[index]);
        }
      }
      this.subSet = builder;
    }
  }

  ngOnInit() {
  }

}
