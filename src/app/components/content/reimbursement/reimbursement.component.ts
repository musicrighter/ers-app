import { Component, OnInit } from '@angular/core';
import { ReimbursementService } from './reimbursement.service';
import { Router } from '@angular/router';
import { Data } from '../../../app.provider';

@Component({
  selector: 'app-reimbursement',
  templateUrl: './reimbursement.component.html',
  styleUrls: ['./reimbursement.component.css']
})
export class ReimbursementComponent implements OnInit {

  constructor(private reimbursementService: ReimbursementService, private router: Router, private data: Data) { }

  amount: string;
  desc: string;
  type = 'Lodging';
  receipt;

  ngOnInit() {
  }

  setFile(event: any) {
    this.receipt = event.target.files[0].name;
  }

  sendReimb() {
    if (this.amount === undefined || this.desc === undefined || this.type === undefined) {
      alert('Please fill out all fields');
    } else {
      this.reimbursementService.submitReimb(this.amount, this.desc, this.type, this.receipt).subscribe(
        resp => {
          console.log('success');

          if (resp.status === 210) {
            this.router.navigate(['login']);
          }
        }
      );
    }
  }
}
