import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class ReimbursementService {

  constructor(private httpClient: HttpClient) { }

  submitReimb(amount: string, desc: string, type: string, receipt: string) {
    const url = 'http://localhost:8080/ERS/userServlet';

    const requestBody = {
      'amount': amount,
      'description': desc,
      'type': type,
      'receipt': receipt
    };

    const header = {
      headers: new HttpHeaders({
        responseType: 'text',
        observe: 'response'
      })
    };
    return this.httpClient.post(url, requestBody, { observe: 'response' });
  }
}
