import { Component, OnInit } from '@angular/core';
import { ManagerService } from './manager.service';
import { Router } from '@angular/router';
import { Data } from '../../../app.provider';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  public body: string[];
  public subSet: string[];
  public status: string;
  public result: string;

  public constructor(private managerService: ManagerService, private router: Router, private data: Data) {
    const obj = JSON.parse(JSON.stringify(this.data.storage));
    this.body = JSON.parse(obj.body);
    this.subSet = this.body;
  }

  setVal(event: any) {
    this.result = event.target.value;
  }

  setSort(event: any) {
    console.log(event.target.value);
    const builder: string[] = [];
    if (event.target.value === 'all') {
      this.subSet = this.body;
    } else {
      for (let index = 0; index < this.body.length; index++) {
        const status = this.body[index].split('status: ')[1].split(',')[0];
        if (status == event.target.value) {
          builder.push(this.body[index]);
        }
      }
      this.subSet = builder;
    }
  }

  sendStat(num) {
    if (this.result == null || this.result === undefined) {
      alert('Please select a reimburesment first');
    } else {
      const id = this.result.split('(')[1].split(')')[0];
      this.managerService.submitStat(num, id).subscribe(
        resp => {
          console.log('success');

          if (resp.status === 210) {
            this.router.navigate(['login']);
          }
        }
      );
    }
  }

  ngOnInit() {
  }

}
