import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class ManagerService {

  constructor(private httpClient: HttpClient) { }

  submitStat(status: string, id: string) {
    const url = 'http://localhost:8080/ERS/managerServlet';

    const requestBody = {
      'status': status,
      'id': id
    };

    const header = {
      headers: new HttpHeaders({
        responseType: 'text',
        observe: 'response'
      })
    };
    return this.httpClient.post(url, requestBody, { observe: 'response' });
  }
}
