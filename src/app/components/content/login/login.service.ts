import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  submitLogin(username: string, password: string) {
    const url = 'http://localhost:8080/ERS/loginServlet';

    const requestBody = {
      'username': username,
      'password': password
  };

  const header = {
    headers: new HttpHeaders({
      responseType: 'text',
      observe: 'response'
    })
  };
    return this.httpClient.post(url, requestBody, { observe: 'response' });
  }
}
