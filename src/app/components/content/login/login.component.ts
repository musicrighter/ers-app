import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { Data } from '../../../app.provider';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router, private data: Data) { }

  username: string;
  password: string;

  ngOnInit() {
  }

  sendLogin() {
    this.loginService.submitLogin(this.username, this.password).subscribe(
      resp => {
        console.log('success');

        this.data.storage = {
          'body': JSON.stringify(resp.body)
        };

        if (resp.status === 210) {
          this.router.navigate(['employee']);
        } else if (resp.status === 211) {
          this.router.navigate(['manager']);
        } else if (resp.status === 212) {
          alert('User does not exist, please try logging in again');
        }
      }
    );
  }

}
