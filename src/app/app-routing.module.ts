import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/content/login/login.component';
import { NgModule } from '@angular/core';
import { EmployeeComponent } from './components/content/employee/employee.component';
import { ManagerComponent } from './components/content/manager/manager.component';
import { ReimbursementComponent } from './components/content/reimbursement/reimbursement.component';

const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'employee', component: EmployeeComponent },
    { path: 'manager', component: ManagerComponent },
    { path: 'reimbursement', component: ReimbursementComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}