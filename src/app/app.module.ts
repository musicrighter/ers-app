import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/content/login/login.component';
import { EmployeeComponent } from './components/content/employee/employee.component';
import { ManagerComponent } from './components/content/manager/manager.component';
import { ReimbursementComponent } from './components/content/reimbursement/reimbursement.component';
import { LoginService } from './components/content/login/login.service';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Data } from './app.provider';
import { ReimbursementService } from './components/content/reimbursement/reimbursement.service';
import { ManagerService } from './components/content/manager/manager.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    EmployeeComponent,
    ManagerComponent,
    ReimbursementComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [LoginService, ReimbursementService, ManagerService, Data],
  bootstrap: [AppComponent]
})
export class AppModule { }
